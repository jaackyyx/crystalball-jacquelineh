package android.hovsepianj.crystalball;

import java.util.Random;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Your wishes will come true.",
                "Your wishes will NEVER come true.",
                "The best way to predict your future is to create it."

        };

    }

    public static Predictions get() {
        if (predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public  String getPrediction(){
        int rnd = new Random().nextInt(answers.length);
        return answers[rnd];
    }

}